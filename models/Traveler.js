class Traveler {
    constructor(name, isHealthy, food) {
        this.name = name;
        this.food = Number(food) || 1;
        this.isHealthy = typeof isHealthy === 'undefined' ? true : isHealthy

    }

    hunt() {
        this.food += 2
    }

    eat() {
        if (this.food === 0) {
            this.isHealthy = false
        }
        else {
            this.food--
        }
    }
}

module.exports = Traveler;