class Wagon {
    constructor(capacity) {
        this.capacity = capacity;
        this.passengers = []
    }

    getAvailableSeatCount() {
        let availableSeats = this.capacity - this.passengers.length;
        return availableSeats;
    }

    join(passenger) {
        if (this.getAvailableSeatCount() > 0) {
            this.passengers.push(passenger)
        }
        return this.passengers
    }

    shouldQuarantine() {
        for (let i = 0; i < this.passengers.length; i++) {
            if (this.passengers[i].isHealthy === "false") {
                return true
            }
        }
        return false
    }

    totalFood() {
        let allFood = 0;
        for (let i = 0; i < this.passengers.length; i++) {
            allFood += this.passengers[i].food;
        }
        return allFood;
    }

}

module.exports = Wagon;