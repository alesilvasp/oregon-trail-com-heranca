const Traveler = require('./Traveler');

class Doctor extends Traveler {
    constructor(name) {
        super(name)
    }

    heal(patient) {
        patient.isHealthy = true
    }
}

module.exports = Doctor;