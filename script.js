class Traveler {
    constructor(name) {
        this.name = name;
        this.food = 1;
        this.isHealthy = true;
    }

    hunt() {
        this.food += 2
        
    }

    eat() {
        if (this.food === 0) {
            this.isHealthy = false
        }
        else {
            this.food--
        }
    }
}

class Doctor extends Traveler {
    constructor(name) {
        super(name)
    }

    heal(patient) {
        patient.isHealthy = true
    }
}

class Hunter extends Traveler {
    constructor(name) {
        super(name)
        this.food = 2
    }
    hunt() {
        this.food += 5
        if (this.food > 0) {
            this.isHealthy = true;
        }
        return this.food
    }

    eat() {
        if (this.food >= 2) {
            this.food -= 2
        } 
        else if (this.food === 1) {
            this.food -= 1
            this.isHealthy = false
        } 
        else {
            this.isHealthy = false
        }
    }

    giveFood(traveler, numOfFoodUnits) {
        if (!this.food < numOfFoodUnits) {
            this.food -= numOfFoodUnits;
            traveler.food = numOfFoodUnits;
        }
    }
}

class Wagon {
    constructor(capacity) {
        this.capacity = capacity;
        this.passengers = []
    }

    getAvailableSeatCount() {
        let availableSeats = this.capacity - this.passengers.length;
        return availableSeats;
    }

    join(passenger) {
        if (this.getAvailableSeatCount() > 0) {
            this.passengers.push(passenger)
        }
        return this.passengers
    }

    shouldQuarantine() {
        let wagonSick = false;
        for (let i = 0; i < this.passengers.length; i++) {
            if (this.passengers[i].isHealthy === false) {
                wagonSick = true
            }
        }
        return wagonSick
    }

    totalFood() {
        let allFood = 0;
        for (let i = 0; i < this.passengers.length; i++) {
            allFood += this.passengers[i].food;
        }
        return allFood;
    }

}
