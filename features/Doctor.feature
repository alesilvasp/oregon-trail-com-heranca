# language: pt

Funcionalidade: Doctor
    Como um doutor
    Eu devo curar os viajantes
    Para que eles se mantenham saudáveis

    Contexto: 
        Dado um doutor com nome de 'Ray'

    Cenário: Recebeu um paciente saudável
        Quando o doutor receber um 'viajante' saudável 
        Então o viajante ficará saudável.

    Cenário: Recebeu um paciente doente
        Quando o doutor receber um 'viajante' doente
        Então o viajante ficará saudável.
    