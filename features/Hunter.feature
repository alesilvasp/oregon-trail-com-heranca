# language: pt

Funcionalidade: Hunter
Como um caçador
Eu devo caçar
Para dar comida a outros viajantes

    Contexto:
        Dado um caçador com nome 'José'
        E sempre começa com 2 comidas
        E sempre começa saudável

    Cenário: Saiu para caçar e conseguiu refeições
        Quando o Hunter sai para caçar 1 vez
        Então a quantidade de refeições deve ser 7

    Cenário: Comeu e ficou saudável
        Quando o Hunter comer 1 vez
        Então a quantidade de refeições deve ser 0
        E o Hunter não ficará doente

    Cenário: Comeu e ficou doente
        Quando o Hunter comer 2 vez
        Então a quantidade de refeições deve ser 0
        E o Hunter ficará doente

    Cenário: Saiu para caçar e deu comida para um viajante
        Quando o Hunter sai para caçar 1 vez
        E um 'viajante' pedir 2 comidas
        Então o Hunter transfere as comidas e ficará com 5 refeições

    Cenário: Saiu para caçar e não deu comida para um viajante
        Quando o Hunter sai para caçar 1 vez
        E um 'viajante' pedir 8 comidas
        Então o Hunter não transfere as comidas e ficará com 7 refeições
    
