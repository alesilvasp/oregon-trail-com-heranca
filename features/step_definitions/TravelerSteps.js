const { Given, When, Then } = require('@cucumber/cucumber')
const assert = require('assert')

const { viajante } = require('../../app')

Given('um Traveler de nome {string}', function (string) {
    viajante.name = string;
});

Given('sempre começa com {int} refeição', function (int) {
    viajante.food = int;
});

Given('sempre começa saudável.', function () {
    viajante.isHealthy = true;
});

/* When */

When('o Traveler sair para caçar {int} vezes', function (int) {
    for (let i = 0; i < int; i++) {
        viajante.hunt();
    }
});

When('o Traveler comer {int} vezes', function (int) {
    for (let i = 0; i < int; i++) {
        viajante.eat();
    }

});

/* Then */

Then('a quantidade de refeições deve ser igual a {int}.', function (int) {
    assert.strictEqual(viajante.food, int);
});

Then('o Traveler não ficará doente.', function () {
    assert.strictEqual(viajante.isHealthy, true)
});

Then('o Traveler ficará doente.', function () {
    assert.strictEqual(viajante.isHealthy, false)
  });