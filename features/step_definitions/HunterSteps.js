const { Given, When, Then } = require('@cucumber/cucumber')
const assert = require('assert')

const Traveler = require('../../models/Traveler')
const { cacador } = require('../../app')
const viajanteSergio = require('../../app')
const Hunter = require('../../models/Hunter')

// let viajante = new Traveler();

/** Given */

Given('um caçador com nome {string}', function (string) {
    cacador.name = string
});

Given('sempre começa com {int} comidas', function (int) {
    cacador.food = int
});

Given('sempre começa saudável', function () {
    cacador.isHealthy = true
});

/** When */

When('o Hunter sai para caçar {int} vez', function (int) {
    for (let i = 0; i < int; i++) {
        cacador.hunt()
    }
});

When('o Hunter comer {int} vez', function (int) {
    for (let i = 0; i < int; i++) {
        cacador.eat()
    }
});

When('um {string} pedir {int} comidas', function (string, int) {
    cacador.giveFood(viajanteSergio, int)
});

/** Then */

Then('a quantidade de refeições deve ser {int}', function (int) {
    assert.strictEqual(cacador.food, int)
});

Then('o Hunter não ficará doente', function () {
    assert.strictEqual(cacador.isHealthy, true)
});

Then('o Hunter ficará doente', function () {
    assert.strictEqual(cacador.isHealthy, false)
});

Then('o Hunter transfere as comidas e ficará com {int} refeições', function (int) {
    assert.strictEqual(cacador.food, int)
});

Then('o Hunter não transfere as comidas e ficará com {int} refeições', function (int) {
    assert.strictEqual(cacador.food, int)
});