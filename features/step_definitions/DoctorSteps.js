const { Given, When, Then } = require('@cucumber/cucumber')
const assert = require('assert')

const Traveler = require('../../models/Traveler')
const { doutor } = require('../../app')

let newPatient = new Traveler();
newPatient.isHealthy = true;

/** Given */

Given('um doutor com nome de {string}', function (string) {
    doutor.name = string
});

/** When */

When('o doutor receber um {string} saudável', function (string) {
    string = newPatient;
    doutor.heal(string)
});

When('o doutor receber um {string} doente', function (string) {
    string = newPatient;
    string.isHealthy = false
    doutor.heal(string)
  });

/** Then */

Then('o viajante ficará saudável.', function () {
    assert.strictEqual(newPatient.isHealthy, true)
});