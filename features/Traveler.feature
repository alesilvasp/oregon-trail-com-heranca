# language:pt

Funcionalidade: Traveler
    Como um Traveler
    Eu devo racionar meus mantimentos
    Para que eu possa seguir a viagem saudável.

    Contexto: 
        Dado um Traveler de nome 'Alex'
        E sempre começa com 1 refeição
        E sempre começa saudável.

    Cenário: Saiu para caçar e conseguir refeições
        Quando o Traveler sair para caçar 1 vezes
        Então a quantidade de refeições deve ser igual a 3.

    Cenário: Comeu e seguiu saudável.
        Quando o Traveler comer 1 vezes
        Então a quantidade de refeições deve ser igual a 0.
        E o Traveler não ficará doente.

    Cenário: Comeu e ficou doente.
        Quando o Traveler comer 2 vezes
        Então a quantidade de refeições deve ser igual a 0.
        E o Traveler ficará doente.

    Cenário: Saiu para caçar, comeu e seguiu saudável.
        Quando o Traveler sair para caçar 2 vezes
        E o Traveler comer 2 vezes
        Então a quantidade de refeições deve ser igual a 3.
        E o Traveler não ficará doente.
